<?php


class Validation {

    // need to create validation for not accepting special characters

    private $hasItem;

    public function __construct() {

    }

    public function checkItems($items) {

        foreach ($items as $item) {

            if ($item == "") {
                $this->hasItem = false;
                break;
            } else {
                $this->hasItem = true;
            }

        }

        return $this->hasItem;
    }

}