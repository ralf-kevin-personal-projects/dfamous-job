<?php 

abstract class Database {

    protected $conn;
    protected $query;
    protected $result;
    protected $response;
    protected $insertedID;

    protected $row;
    protected $rows;

    public function __construct(){
    }

    protected function createConn() {

        $this->conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if (mysqli_connect_errno()) {
            echo "Failed to connect to Database" . mysqli_connect_errno();
        }

        return $this->conn;
    }

    protected function query($stmt) {

        $this->query = $stmt;
        $this->result = mysqli_query($this->conn, $this->query);
    }

    protected function resultSet() {

        if ($this->result) {

            if (mysqli_num_rows($this->result) > 0) {
                
                $this->rows = array();

                while ($this->row = mysqli_fetch_assoc($this->result)) $this->rows[] = $this->row;
                $this->response['success'] = true;
                $this->response['result'] = $this->rows;

            } else {
                $this->response['success'] = false;
                $this->response['result'] = "No Result";
            }

        } else {
            $this->response['success'] = false;
            $this->response['result'] = "error in query " . $this->conn->errno . " " . $this->conn->error;
        }

        return $this->response;
    }

    protected function insertData() {
        $this->result = mysqli_affected_rows($this->conn);
        
        if ($this->result) {
            $this->response['success'] = true;
            $this->response['result'] = "Successfuly Added!";
        } else {
            $this->response['success'] = false;
            $this->response['result'] = "error in query " . $this->conn->errno . " " . $this->conn->error;
        }

        return $this->response;
    }

    protected function updateData() {
        $this->result = mysqli_affected_rows($this->conn);

        if ($this->result) {
            $this->response['success'] = true;
            $this->response['result'] = "Successfuly Updated!";
        } else {
            $this->response['success'] = false;
            $this->response['result'] = "error in query " . $this->conn->errno . " " . $this->conn->error;
        }

        return $this->response;
    }

    protected function getLastInsertedID() {

       $this->insertedID = $this->conn->insert_id;
       return $this->insertedID;

    }
    
}