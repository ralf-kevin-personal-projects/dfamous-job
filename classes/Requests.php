<?php

require('../config.php');
require('Database.php');
require('Validation.php');
require('CompanyPost.php');
require('Application.php');

$json_response = array();
$validation = new Validation();

if (isset($_POST["request"])) {
    $req = $_POST["request"];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    // if ($validation->checkItems($_POST)) {
        
        switch ($req) {       
            case "fetch_job_post":
                $companyPost = new CompanyPost(null, "fetch_job_post");
                $json_response = $companyPost->fetchAll();            
            break;

            case "apply":
                $params = [$_POST, $_FILES];
                $application = new Application($params, "apply");
                $json_response = $application->sendApplication();
            break;

            default:
                $json_response["success"] = false;
                $json_response["result"] = "Unknown Request";
            break;
        }
        
    // } else {
    //     $json_response["success"] = false;
    //     $json_response["result"] = "Empty Post Value";        
    // }

} else {
    $json_response["success"] = false;
    $json_response["result"] = "Empty Request Value";
}

echo json_encode($json_response);