<?php

class Application extends Database {

    private $given_name;
    private $sur_name;
    private $middle_name;
    private $address;
    private $number;
    private $number2;
    private $bday;
    private $bdate;
    private $gender;
    private $citizenship;
    private $civil;
    private $height;
    private $weight;
    private $spouse;
    private $spouse_work;
    private $spouse_num;
    private $spouse_num2;

    private $child_name;
    private $child_age;
    private $child_name2;
    private $child_age2;
    private $child_name3;
    private $child_age3;
    private $child_name4;
    private $child_age4;

    private $father_name;
    private $father_work;
    private $father_num;

    private $mother_name;
    private $mother_work;
    private $mother_num;
    private $parent_add;


    private $emergency_name;
    private $emergency_add;
    private $emergency_num;

    private $schl_pri;
    private $schl_pri_yr;

    private $schl_hs;
    private $schl_hs_yr;

    private $schl_college;
    private $schl_college_course;
    private $schl_college_yr;

    private $company;
    private $company_pos;
    private $company_frm;
    private $company_to;

    private $company2;
    private $company2_pos;
    private $company2_frm;
    private $company2_to;

    private $company3;
    private $company3_pos;
    private $company3_frm;
    private $company3_to;

    private $sss;
    private $philhealth;
    private $pagibig;
    private $tin;


    private $pdf_name;
    private $tmp_name;

    private $company_id;


    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "apply":    
                $this->given_name = $args[0]["given_name"];
                $this->sur_name = $args[0]["sur_name"];
                $this->middle_name = $args[0]["middle_name"];
                $this->address = $args[0]["address"];
                $this->number = $args[0]["number"];
                $this->number2 = $args[0]["number2"];
                $this->bday = $args[0]["bday"];
                $this->bdate = $args[0]["bdate"];
                $this->gender = $args[0]["gender"];
                $this->citizenship = $args[0]["citizenship"];
                $this->civil = $args[0]["civil"];
                $this->height = $args[0]["height"];
                $this->weight = $args[0]["weight"];
                $this->spouse = $args[0]["spouse"];
                $this->spouse_work = $args[0]["spouse_work"];
                $this->spouse_num = $args[0]["spouse_num"];
                $this->spouse_num2 = $args[0]["spouse_num2"];

                $this->child_name = $args[0]["child_name"];
                $this->child_age = $args[0]["child_age"];
                $this->child_name2 = $args[0]["child_name2"];
                $this->child_age2 = $args[0]["child_age2"];
                $this->child_name3 = $args[0]["child_name3"];
                $this->child_age3 = $args[0]["child_age3"];
                $this->child_name4 = $args[0]["child_name4"];
                $this->child_age4 = $args[0]["child_age4"];

                $this->father_name = $args[0]["father_name"];
                $this->father_work = $args[0]["father_work"];
                $this->father_num = $args[0]["father_num"];

                $this->mother_name = $args[0]["mother_name"];
                $this->mother_work = $args[0]["mother_work"];
                $this->mother_num = $args[0]["mother_num"];
                $this->parent_add = $args[0]["parent_add"];

                $this->emergency_name = $args[0]["emergency_name"];
                $this->emergency_add = $args[0]["emergency_add"];
                $this->emergency_num = $args[0]["emergency_num"];

                $this->schl_pri = $args[0]["schl_pri"];
                $this->schl_pri_yr = $args[0]["schl_pri_yr"];

                $this->schl_hs = $args[0]["schl_hs"];
                $this->schl_hs_yr = $args[0]["schl_hs_yr"];

                $this->schl_college = $args[0]["schl_college"];
                $this->schl_college_course = $args[0]["schl_college_course"];
                $this->schl_college_yr = $args[0]["schl_college_yr"];

                $this->company = $args[0]["company"];
                $this->company_pos = $args[0]["company_pos"];
                $this->company_frm = $args[0]["company_frm"];
                $this->company_to = $args[0]["company_to"];

                $this->company2 = $args[0]["company2"];
                $this->company2_pos = $args[0]["company2_pos"];
                $this->company2_frm = $args[0]["company2_frm"];
                $this->company2_to = $args[0]["company2_to"];

                $this->company3 = $args[0]["company3"];
                $this->company3_pos = $args[0]["company3_pos"];
                $this->company3_frm = $args[0]["company3_frm"];
                $this->company3_to = $args[0]["company3_to"];

                $this->sss = $args[0]["sss"];
                $this->philhealth = $args[0]["philhealth"];
                $this->pagibig = $args[0]["pagibig"];
                $this->tin = $args[0]["tin"];

                $this->company_id = $args[0]["company_id"];

                $this->pdf_name = $args[1]["pdf"]["name"];
                $this->tmp_name = $args[1]["pdf"]["tmp_name"];
            break;

            default:

            break;
        }
    }


    public function sendApplication() {

        $this->createConn();

        if ($this->uploadPDF()) {
            $this->query("INSERT INTO applicant 
                        (app_fname, app_lname, app_mname, app_address, app_mobile1,
                        app_mobile2, app_bday, app_bplace, app_gender, app_citizenship,
                        app_civil, app_height, app_weight, app_spouse, app_spouse_occupation,
                        app_spouse_mobile1, app_spouse_mobile2, app_children1_name, app_children1_age, app_children2_name,
                        app_children2_age, app_children3_name, app_children3_age, app_children4_name, app_children4_age,
                        app_father, app_father_occupation, app_father_mobile, app_mother, app_mother_occupation,
                        app_mother_mobile, app_parent_add, app_emergency_person, app_emergency_add, app_emergency_mobile,
                        app_schl_pri, app_schl_pri_yr, app_schl_hs, app_schl_hs_yr, app_schl_college,
                        app_schl_course, app_schl_course_yr, app_cmpny1, app_cmpny1_pos, app_cmpny1_from,
                        app_cmpny1_to, app_cmpny2, app_cmpny2_pos, app_cmpny2_from, app_cmpny2_to,
                        app_cmpny3, app_cmpny3_pos, app_cmpny3_from, app_cmpny3_to, app_sss,
                        app_philhealth, app_pagibig, app_tin, company_id, app_cv)
                        
                        values 
                        ('". $this->given_name ."', '". $this->sur_name ."', '". $this->middle_name ."', '". $this->address ."', '". $this->number ."',
                        '". $this->number2 ."', '". $this->bday ."', '". $this->bdate ."', '". $this->gender ."', '". $this->citizenship ."' ,              
                        '". $this->civil ."', '". $this->height ."', '". $this->weight ."', '". $this->spouse ."', '". $this->spouse_work ."',
                        '". $this->spouse_num ."', '". $this->spouse_num2 ."', '". $this->child_name ."', '". $this->child_age ."', '". $this->child_name2 ."',
                        '". $this->child_age2 ."', '". $this->child_name3 ."', '". $this->child_age3 ."', '". $this->child_name4 ."', '". $this->child_age4 ."',
                        '". $this->father_name ."', '". $this->father_work ."', '". $this->father_num ."', '". $this->mother_name ."', '". $this->mother_work ."',
                        '". $this->mother_num ."', '". $this->parent_add ."', '". $this->emergency_name ."', '". $this->emergency_add ."', '". $this->emergency_num ."',
                        '". $this->schl_pri ."', '". $this->schl_pri_yr ."', '". $this->schl_hs ."', '". $this->schl_hs_yr ."', '". $this->schl_college ."',
                        '". $this->schl_college_course ."', '". $this->schl_college_yr ."', '". $this->company ."', '". $this->company_pos ."', '". $this->company_frm ."',
                        '". $this->company_to ."', '". $this->company2 ."', '". $this->company2_pos ."', '". $this->company2_frm ."', '". $this->company2_to ."',
                        '". $this->company3 ."', '". $this->company3_pos ."', '". $this->company3_frm ."', '". $this->company3_to ."', '". $this->sss ."',
                        '". $this->philhealth ."', '". $this->pagibig ."', '". $this->tin ."', '". $this->company_id ."', '". $this->pdf_name ."'
                        )
                        ");

            $hasResult = $this->insertData();

            if ($hasResult["success"] == true) {

                $this->res["success"] = true;
                $this->res["result"] = $hasResult["result"];

            } else {

                $this->res["success"] = false;
                $this->res["result"] = $hasResult["result"];

            }
            
        } else {
            $this->res["success"] = false;
            $this->res["result"] = "Some went wrong when uploading your resume/cv";
        }
        return $this->res;

    }

    private function uploadPDF() {

        $loc = "../uploads/";
        $uploaded = false;

        if ($this->pdf_name != null) {
            $this->pdf_name = rand(100, 999999). $this->pdf_name;
            $loc = $loc . basename($this->pdf_name);

            if (move_uploaded_file($this->tmp_name, $loc)) {
                $uploaded = true;
            } else {
                $uploaded = false;
            }
        } else {
            $uploaded = false;            
        }

    return $uploaded;
    }

}