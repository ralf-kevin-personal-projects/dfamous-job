<?php

class CompanyPost extends Database {

    private $id;
    private $companyId;

    private $status;


    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_job_post":
            break;

            default:

            break;
        }
    }


    public function updateJobPost() {

        $this->createConn();

        $this->query("UPDATE company_posting  SET
                    post_status = '". $this->status ."'
                    WHERE post_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM company_posting as post 
                    LEFT JOIN acct_company as ac
                    on post.company_id = ac.company_id
                    WHERE post.post_status = 'Approved'
                    ORDER BY post.post_id DESC
                    ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

}