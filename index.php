<?php include 'headers/header.php'; ?>


  <div class="nav nav-bar">
    <div class="row">
      <div class="col-md-4">
        <h3>D'FAMOUS MANPOWER AND GENERAL SERVICE INC.</h3>      
      </div>
      <div class="col-md-4 offset-md-4">
        <div class="form-group">
          <input id="search" type="text" class="form-control search" placeholder="Search Job.." />
        </div>
      </div>
      <!-- <div class="col-md-2">
        <div class="form-group">
          <label><i class="fas fa-users">Company Partner</i></label>
          <label>Employee</label>
        </div>
      </div> -->

    </div>
  </div>

  <div class="container">

      <div id="job-lists">

      </div>


      <div class="row load-more">
        <button class="btn btn-md btn-success form-control">Load More</button>
      </div>
  </div>


<?php include 'headers/footer.php'; ?>
<script>
  $(document).ready(function() {

      loadData();

      $("#search").keyup(function() {
        alert($(this).val());
      });

      $(document).on("click", "#view-details", function() {

        var title = $(this).attr("data-title");
        var company = $(this).attr("data-company");
        var loc = $(this).attr("data-loc");
        var educ = $(this).attr("data-educ");
        var exp = $(this).attr("data-exp");
        var salary = $(this).attr("data-salary");
        var jcat = $(this).attr("data-jcat");
        var vacancy = $(this).attr("data-vacancy");
        var company_desc = $(this).attr("data-comp-desc");
        var desc = $(this).attr("data-desc");
        var company_id = $(this).attr("data-comp-id");
        var id = $(this).attr("data-id");

        if (typeof(Storage) !== "undefined") {

            sessionStorage.setItem("title", title);
            sessionStorage.setItem("company", company);
            sessionStorage.setItem("loc", loc);
            sessionStorage.setItem("educ", educ);
            sessionStorage.setItem("exp", exp);
            sessionStorage.setItem("salary", salary);
            sessionStorage.setItem("jcat", jcat);
            sessionStorage.setItem("vacancy", vacancy);
            sessionStorage.setItem("company_desc", company_desc);
            sessionStorage.setItem("desc", desc);
            sessionStorage.setItem("company_id", company_id);
            sessionStorage.setItem("id", id);

            location.href = "description.php";
        } else {
          alert("Your Browser is not supported to our function, please download better browser")
        }

      });



      function loadData() {

        var fd = new FormData();

        fd.append("request", "fetch_job_post");

        $.ajax({
            type: "POST",
            url: "classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){
                // alert(res.result);
                populateData(res.result);
                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
      }

      function populateData(datas) {

          var tmpl = "";

          for (var i = 0; i < datas.length; i++) {

            var id = datas[i]["post_id"];
            var company_id = datas[i]["company_id"];

            var title = datas[i]["post_title"];
            var company = datas[i]["company_name"];

            var location = datas[i]["company_location"];
            var educ = datas[i]["post_educ"];
            var exp = datas[i]["post_exp"];
            var salary = datas[i]["post_salary"];
            var jcat = datas[i]["post_cat"];
            var vacancy = datas[i]["post_vacancy"];

            var company_desc = datas[i]["company_desc"];
            var desc = datas[i]["post_desc"];
            var status = datas[i]["post_status"];
            var date = datas[i]["publish_date"];


            tmpl += "<div class='job-post'>"+
                      "<div class='row'>"+
                          "<div class='col-md-12'>"+
                            "<h3>"+ title +"</h3>"+
                            "<span><b>Company:</b> "+ company +"</span>"+
                          "</div>"+
                      "</div>"+
                      "<div class='row'>"+
                          "<div class='col-md-8'>"+
                            "<span><b>Location:</b> "+ location +"</span>"+
                          "</div>"+
                          "<div class='col-md-8'>"+
                            "<span><b>Education Attainment:</b> "+ educ +"</span>"+
                          "</div>"+            
                          "<div class='col-md-8'>"+
                            "<span><b>Expertise:</b> "+ exp +"</span>"+
                          "</div>"+
                      "</div>"+
                      "<div class='row'>"+
                          "<div class='col-md-10'>"+
                            "<span><b>Salary:</b> "+ salary +"</span>"+
                          "</div>"+
                          "<div class='col-md-10'>"+
                            "<span><b>Job Category:</b> "+ jcat +"</span>"+
                          "</div>"+           
                      "</div>"+
                      "<div class='row'>"+
                          "<div class='col-md-10'>"+
                            "<span><b>Vacancy:</b> "+ vacancy +"</span>"+
                          "</div>"+
                          "<div class='col-md-2'>"+
                            "<button id='view-details' class='btn form-control btn-md btn-outline-success' "+
                              "data-title= '"+ title +"' "+
                              "data-company= '"+ company +"' "+
                              "data-loc= '"+ location +"' "+
                              "data-educ= '"+ educ +"' "+
                              "data-exp= '"+ exp +"' "+
                              "data-salary= '"+ salary +"' "+
                              "data-jcat= '"+ jcat +"' "+
                              "data-vacancy= '"+ vacancy +"' "+                            
                              "data-comp-desc= '"+ company_desc +"' "+
                              "data-desc= '"+ desc +"' "+
                              "data-comp-id= '"+ company_id +"' "+
                              "data-id= '"+ id +"' > "+
                              "View Details</button>"+
                          "</div>"+
                      "</div>"+
                  "</div>";

          }

          $("#job-lists").append(tmpl);

      }

  });
</script>