<?php include 'headers/header.php'; ?>

  <div class="nav nav-bar desc">
    <div class="row">
      <div class="col-md-12">
        <h3 id="header-company"></h3>      
      </div>
    </div>
  </div>

    <div id="main" class="container">
    </div>


<!-- Modal -->
<div class="modal fade" id="jpModal" tabindex="-1" role="dialog" aria-labelledby="jpModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="jpModalTitle">Application Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Personal Information</h6>
                <div class="row">
                        <div class="col-md-4">
                            <label>Given Name</label>                                                                
                            <div class="form-group">
                                <input id="given_name" type="text" class="form-control" placeholder="Given name"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Sur Name</label>                                                                                        
                            <div class="form-group">                        
                                <input id="sur_name" type="text" class="form-control" placeholder="Sur name"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Middle Name</label>                                                                                        
                            <div class="form-group">                                                
                                <input id="middle_name" type="text" class="form-control" placeholder="Middle name"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">                                                                    
                            <label>Home Address</label>                        
                            <input id="address" type="text" class="form-control" placeholder="Home Address"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input id="number" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 2</label>                                                                  
                            <input id="number2" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Date of Birth</label>                                                                  
                            <input id="bday" type="date" class="form-control" placeholder="Date of Birth"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Place of Birth</label>                                      
                            <input id="bdate" type="number" class="form-control" placeholder="Place of Birth"/>
                        </div>                       
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gender</label>                                                                  
                            <select id="gender" class="form-control">
                                <option value="">Select Gender</option>
                                <option value="">Male</option>                            
                                <option value="">Female</option>                                                        
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Citizenship</label>                                      
                            <input id="citizenship" type="text" class="form-control" placeholder="Citizenship"/>
                        </div>                       
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Civil Status</label>
                            <select id="civil" class="form-control">
                                <option value="">Select Civil Status</option>
                                <option value="">Single</option>                            
                                <option value="">Married</option>                                                        
                                <option value="">Widowed</option>
                            </select>
                        </div>
                    </div>                
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Height</label>                                                                  
                            <input id="height" type="text" class="form-control" placeholder="Height"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Weight</label>                                      
                            <input id="weight" type="text" class="form-control" placeholder="Weight"/>
                        </div>                       
                    </div>             
                </div>

            </div>
            <hr/>


            <div class="form-group">
            
                <h6>* Skip this if you select civil status as single</h6>
                <div class="row">
                        <div class="col-md-8">
                            <label>Name of Spouse</label>                                                                
                            <div class="form-group">
                                <input id="spouse" type="text" class="form-control" placeholder="Name of Spouse"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="spouse_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input id="spouse_num" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Mobile number 2</label>                                                                  
                            <input id="spouse_num2" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name2" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age2" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name3" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age3" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name4" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age4" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>                                                


            </div>            
            <hr/>


            <div class="form-group">
            
                <h6>Parent Information</h6>
                <div class="row">
                        <div class="col-md-6">
                            <label>Name of Father</label>                                                                
                            <div class="form-group">
                                <input id="father_name" type="text" class="form-control" placeholder="Name of Father"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="father_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="father_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <label>Name of Mother</label>                                                                
                            <div class="form-group">
                                <input id="mother_name" type="text" class="form-control" placeholder="Name of Mother"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="mother_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="mother_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>            
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Address</label>                                                                  
                            <input id="parent_add" type="text" class="form-control" placeholder="Address"/>
                        </div>                       
                    </div>
                </div>
            </div>
            <hr/>

            <div class="form-group">
            
                <h6>For Emergency</h6>
                <div class="row">
                        <div class="col-md-12">
                            <label>Person to be notified in case of Emergency</label>                                                                
                            <div class="form-group">
                                <input id="emergency_name" type="text" class="form-control" placeholder="Person to be notified in case of Emergency"/>
                            </div>
                        </div>                    
                </div>
                <div class="row">
                        <div class="col-md-8">
                            <label>Address</label>                                                                
                            <div class="form-group">
                                <input id="emergency_add" type="text" class="form-control" placeholder="Address"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="emergency_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>            


            </div>
            <hr/>

            <div class="form-group">
                
                <h6>Educational Background</h6>
                <div class="row">
                        <div class="col-md-9">
                            <label>Name of School (Primary)</label>                                            
                            <div class="form-group">
                                <input id="schl_pri" type="text" class="form-control" placeholder="Name of School (Primary)"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_pri_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-9">
                            <label>Name of School (High School)</label>                                            
                            <div class="form-group">
                                <input id="schl_hs" type="text" class="form-control" placeholder="Name of School (High School)"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_hs_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>                    
                </div>            
                <div class="row">
                        <div class="col-md-7">
                            <label>Name of School (College)</label>                                            
                            <div class="form-group">
                                <input id="schl_college" type="text" class="form-control" placeholder="Name of School (College)"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Course</label>                                            
                            <div class="form-group">
                                <input id="schl_college_course" type="text" class="form-control" placeholder="Course"/>
                            </div>
                        </div>                                         
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_college_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>
                </div>


            </div>
            <hr/>

            <div class="form-group">
                
                <h6>Employment History</h6>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company2" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company3" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                
            </div>
            <hr/>

            <div class="form-group">
                
                <h6></h6>
                <div class="row">
                        <div class="col-md-6">
                            <label>SSS #</label>                                            
                            <div class="form-group">
                                <input id="sss" type="text" class="form-control" placeholder="SSS #"/>
                            </div>
                        </div>                    
                        <div class="col-md-6">
                            <label>Phil Health #</label>                                                                                        
                            <div class="form-group">                        
                                <input id="philhealth" type="text" class="form-control" placeholder="Phil Health #"/>
                            </div>
                        </div>                                              
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <label>Pag Ibig #</label>                                            
                            <div class="form-group">
                                <input id="pagibig" type="text" class="form-control" placeholder="Pag Ibig #"/>
                            </div>
                        </div>                    
                        <div class="col-md-6">
                            <label>TIN #</label>                                                                                        
                            <div class="form-group">                        
                                <input id="tin" type="text" class="form-control" placeholder="TIN #"/>
                            </div>
                        </div>                                              
                </div>


            </div>
            <hr/>

            <div class="form-group">
                
                <h6></h6>
                <div class="row">
                    <div class="col-md-12">
                        <input id="agree" type="checkbox"/>
                        <span>
                            I certify that my answers are true and complete to the best of my knowledge.
                            If this application leads to employment, I understand the false or misleading
                            Information in my application or interview may result in my employment being
                            terminated.
                        </span>
                    </div>                                              
                </div>

            </div>
            <hr/>

            <div class="form-group">
                
                <h6>Please attach your Resume/CV</h6>
                <div class="row">
                    <div class="col-md-12">
                        <input id="filecv" type="file" accept="application/pdf" placeholder="Please attached your Resume"/>
                    </div>                                              
                </div>

            </div>
            <hr/>
            
            
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="apply" type="button" class="btn btn-success">Submit Application</button>
      </div>
    </div>
  </div>
</div>


<?php include 'headers/footer.php'; ?>


<script>
    $(document).ready(function() {

        loadSessions();

        $(document).on("click", "#send-application", function(){
            $("#jpModal").modal("show");
        });


        function loadSessions() {
            if (typeof(Storage) !== "undefined") {
                
                if (sessionStorage.getItem("company") == null) {
                    location.href = "index.php";
                } else {
                    $("#header-company").text(sessionStorage.getItem("company"));
                    populateSession(sessionStorage);
                }

            } else {
                alert("Your Browser is not supported to our function, please download better browser")
            }
        }

        function populateSession(sessions) {

            var tmpl = "";

                tmpl += "<div class='job-post card'>"+
                            "<div class='row'>"+
                                "<div class='col-md-12'>"+
                                    "<h3>"+ sessions.getItem("title") +"</h3>"+
                                "</div>"+
                            "</div>"+
                            "<div class='row'>"+
                                "<div class='col-md-8'>"+
                                    "<span><b>Location:</b> "+ sessions.getItem("loc") +"</span>"+
                                "</div>"+
                                "<div class='col-md-8'>"+
                                    "<span><b>Educational Attainment:</b> "+ sessions.getItem("educ") +"</span>"+
                                "</div>"+
                                "<div class='col-md-8'>"+
                                    "<span><b>Expertise:</b> "+ sessions.getItem("exp") +"</span>"+
                                "</div>"+
                            "</div>"+
                            "<div class='row'>"+
                                "<div class='col-md-10'>"+
                                    "<span><b>Salary:</b> "+ sessions.getItem("salary") +"</span>"+
                                "</div>"+
                                "<div class='col-md-10'>"+
                                    "<span><b>Job Category:</b> "+ sessions.getItem("jcat") +"</span>"+
                                "</div>"+
                                "<div class='col-md-10'>"+
                                    "<span><b>Vacancy:</b> "+ sessions.getItem("vacancy") +"</span>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class='job-post card'>"+
                            "<div class='row'>"+
                                "<div class='col-md-12'>"+
                                    "<h5>About Company A</h5>"+
                                "</div>"+
                            "</div>"+
                            "<div class='row'>"+
                                "<div class='col-md-12'>"+
                                    "<span> "+ sessions.getItem("company_desc") +" </span>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+

                        "<div class='job-post card'>"+
                            "<div class='row'>"+
                                "<div class='col-md-12'>"+
                                    "<h5>Description</h5>"+
                                "</div>"+
                            "</div>"+
                            "<div class='row'>"+
                                "<div class='col-md-12'>"+
                                    "<span> "+ sessions.getItem("desc") +" </span>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                        "<div class='row load-more'>"+
                            "<button id='send-application' class='form-control btn btn-md btn-success' >"+
                            "SEND AN APPLICATION</button>"+
                        "</div>";

            $("#main").append(tmpl);                        
        }



        $("#apply").hide();

        $("#agree").click(function() {
            if ($(this).is(":checked")) {
                $("#apply").show();
            } else {
                $("#apply").hide();
            }
        });

        $("#apply").click(function() {
            var gname = $("#given_name").val();
            var sname = $("#sur_name").val();
            var mname = $("#middle_name").val();
            var address = $("#address").val();
            var number = $("#number").val();
            var number2 = $("#number2").val();
            var bday = $("#bday").val();
            var bdate = $("#bdate").val();
            var gender = $("#gender").val();
            var citizenship = $("#citizenship").val();
            var civil = $("#civil").val();
            var height = $("#height").val();
            var weight = $("#weight").val();
            var spouse = $("#spouse").val();
            var spouse_work = $("#spouse_work").val();
            var spouse_num = $("#spouse_num").val();
            var spouse_num2 = $("#spouse_num2").val();

            var child_name = $("#child_name").val();
            var child_age = $("#child_age").val();
            var child_name2 = $("#child_name2").val();
            var child_age2 = $("#child_age2").val();
            var child_name3 = $("#child_name3").val();
            var child_age3 = $("#child_age3").val();
            var child_name4 = $("#child_name4").val();
            var child_age4 = $("#child_age4").val();

            var father_name = $("#father_name").val();
            var father_work = $("#father_work").val();
            var father_num = $("#father_num").val();

            var mother_name = $("#mother_name").val();
            var mother_work = $("#mother_work").val();
            var mother_num = $("#mother_num").val();
            var parent_add = $("#parent_add").val();           

            var emergency_name = $("#emergency_name").val();
            var emergency_add = $("#emergency_add").val();
            var emergency_num = $("#emergency_num").val();

            var schl_pri = $("#schl_pri").val();
            var schl_pri_yr = $("#schl_pri_yr").val();
            var schl_hs = $("#schl_hs").val();
            var schl_hs_yr = $("#schl_hs_yr").val();
            var schl_college = $("#schl_college").val();
            var schl_college_course = $("#schl_college_course").val();
            var schl_college_yr = $("#schl_college_yr").val();

            var company = $("#company").val();
            var company_pos =  $("#company_pos").val();
            var company_frm = $("#company_frm").val();
            var company_to = $("#company_to").val();
            var company2 = $("#company2").val();
            var company2_pos = $("#company2_pos").val();
            var company2_frm = $("#company2_frm").val();
            var company2_to = $("#company2_to").val();
            var company3 = $("#company3").val();
            var company3_pos = $("#company3_pos").val();
            var company3_frm = $("#company3_frm").val();
            var company3_to = $("#company3_to").val();
            
            var sss = $("#sss").val();
            var philhealth = $("#philhealth").val();
            var pagibig = $("#pagibig").val();
            var tin = $("#tin").val();

            var pdf = document.getElementById("filecv").files[0];

            var values = [gname, sname, mname, address, number,
                        number2, bday, bdate, gender, citizenship,
                        civil, height, weight, spouse, spouse_work,
                        spouse_num, spouse_num2, child_name, child_age, child_name2,
                        child_age2, child_name3, child_age3, child_name4, child_age4,
                        father_name, father_work, father_num, mother_name, mother_work,
                        mother_num, parent_add, emergency_name, emergency_add, emergency_num,
                        schl_pri, schl_pri_yr, schl_hs, schl_hs_yr, schl_college,
                        schl_college_course, schl_college_yr, company, company_pos, company_frm,
                        company_to, company2, company2_pos, company2_frm, company2_to,
                        company3, company3_pos, company3_frm, company3_to, sss,
                        philhealth, pagibig, tin];

            sendApplication(values, pdf);
        });

        function sendApplication(values, pdf) {
            
            var company_id = sessionStorage.getItem("company_id");
            var fd = new FormData();

            fd.append("given_name", values[0]);
            fd.append("middle_name", values[1]);
            fd.append("sur_name", values[2]);
            fd.append("address", values[3]);
            fd.append("number", values[4]);
            fd.append("number2", values[5]);
            fd.append("bday", values[6]);
            fd.append("bdate", values[7]);
            fd.append("gender", values[8]);
            fd.append("citizenship", values[9]);
            fd.append("civil", values[10]);
            fd.append("height", values[11]);
            fd.append("weight", values[12]);
            fd.append("spouse", values[13]);
            fd.append("spouse_work", values[14]);
            fd.append("spouse_num", values[15]);
            fd.append("spouse_num2", values[16]);
            fd.append("child_name", values[17]);
            fd.append("child_age", values[18]);
            fd.append("child_name2", values[19]);
            fd.append("child_age2", values[20]);
            fd.append("child_name3", values[21]);
            fd.append("child_age3", values[22]);
            fd.append("child_name4", values[23]);
            fd.append("child_age4", values[24]);
            fd.append("father_name", values[25]);
            fd.append("father_work", values[26]);
            fd.append("father_num", values[27]);
            fd.append("mother_name", values[28]);
            fd.append("mother_work", values[29]);
            fd.append("mother_num", values[30]);
            fd.append("parent_add", values[31]);
            fd.append("emergency_name", values[32]);
            fd.append("emergency_add", values[33]);
            fd.append("emergency_num", values[34]);
            fd.append("schl_pri", values[35]);
            fd.append("schl_pri_yr", values[36]);
            fd.append("schl_hs", values[37]);
            fd.append("schl_hs_yr", values[38]);
            fd.append("schl_college", values[39]);
            fd.append("schl_college_course", values[40]);
            fd.append("schl_college_yr", values[41]);
            fd.append("company", values[42]);
            fd.append("company_pos", values[43]);
            fd.append("company_frm", values[44]);
            fd.append("company_to", values[45]);
            fd.append("company2", values[46]);
            fd.append("company2_pos", values[47]);
            fd.append("company2_frm", values[48]);
            fd.append("company2_to", values[49]);
            fd.append("company3", values[50]);
            fd.append("company3_pos", values[51]);
            fd.append("company3_frm", values[52]);
            fd.append("company3_to", values[53]);
            fd.append("sss", values[54]);
            fd.append("philhealth", values[55]);
            fd.append("pagibig", values[56]);
            fd.append("tin", values[57]);
            fd.append("pdf", pdf);
            fd.append("company_id", company_id);
            fd.append("request", "apply");

            $.ajax({
                type: "POST",
                url: "classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    alert("response: " + res.result)
                    console.log(res);

                }, error: function() {
                    alert("error handler")
                }
            });
        }





    });
</script>